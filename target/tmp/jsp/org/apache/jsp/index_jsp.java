package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; UTF-8;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--\n");
      out.write("\n");
      out.write(" Licensed to the Apache Software Foundation (ASF) under one\n");
      out.write(" or more contributor license agreements.  See the NOTICE file\n");
      out.write(" distributed with this work for additional information\n");
      out.write(" regarding copyright ownership.  The ASF licenses this file\n");
      out.write(" to you under the Apache License, Version 2.0 (the\n");
      out.write(" \"License\"); you may not use this file except in compliance\n");
      out.write(" with the License.  You may obtain a copy of the License at\n");
      out.write("\n");
      out.write("   http://www.apache.org/licenses/LICENSE-2.0\n");
      out.write("\n");
      out.write(" Unless required by applicable law or agreed to in writing,\n");
      out.write(" software distributed under the License is distributed on an\n");
      out.write(" \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY\n");
      out.write(" KIND, either express or implied.  See the License for the\n");
      out.write(" specific language governing permissions and limitations\n");
      out.write(" under the License.\n");
      out.write("\n");
      out.write("-->\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n");
      out.write("<title>Apache Olingo - OData2 Library</title>\n");
      out.write("<style type=\"text/css\">\n");
      out.write("body { font-family: Arial, sans-serif; font-size: 13px; line-height: 18px;\n");
      out.write("       color: blue; background-color: #ffffff; }\n");
      out.write("a { color: blue; text-decoration: none; }\n");
      out.write("a:focus { outline: thin dotted #4076cb; outline-offset: -1px; }\n");
      out.write("a:hover, a:active { outline: 0; }\n");
      out.write("a:hover { color: #404a7e; text-decoration: underline; }\n");
      out.write("h1, h2, h3, h4, h5, h6 { margin: 9px 0; font-family: inherit; font-weight: bold;\n");
      out.write("                         line-height: 1; color: blue; }\n");
      out.write("h1 { font-size: 36px; line-height: 40px; }\n");
      out.write("h2 { font-size: 30px; line-height: 40px; }\n");
      out.write("h3 { font-size: 24px; line-height: 40px; }\n");
      out.write("h4 { font-size: 18px; line-height: 20px; }\n");
      out.write("h5 { font-size: 14px; line-height: 20px; }\n");
      out.write("h6 { font-size: 12px; line-height: 20px; }\n");
      out.write(".logo { float: right; }\n");
      out.write("ul { padding: 0; margin: 0 0 9px 25px; }\n");
      out.write("ul ul { margin-bottom: 0; }\n");
      out.write("li { line-height: 18px; }\n");
      out.write("hr { margin: 18px 0;\n");
      out.write("     border: 0; border-top: 1px solid #cccccc; border-bottom: 1px solid #ffffff; }\n");
      out.write("table { border-collapse: collapse; border-spacing: 10px; }\n");
      out.write("th, td { border: 1px solid; padding: 20px; }\n");
      out.write(".code { font-family: \"Courier New\", monospace; font-size: 13px; line-height: 18px; }\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    <h1>Apache Olingo - OData2 Library</h1>\n");
      out.write("    <hr />\n");
      out.write("    <h2>Cars Annotation Service</h2>\n");
      out.write("    <table>\n");
      out.write("        <tr>\n");
      out.write("            <td valign=\"top\">\n");
      out.write("                <h3>Service Document and Metadata</h3>\n");
      out.write("                <ul>\n");
      out.write("                    <li><a href=\"odata.svc?_wadl\" target=\"_blank\">wadl</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/\" target=\"_blank\">service document</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/$metadata\" target=\"_blank\">metadata</a></li>\n");
      out.write("                </ul>\n");
      out.write("                <h3>EntitySets</h3>\n");
      out.write("                <ul>\n");
      out.write("                    <li><a href=\"odata.svc/Manufacturers\" target=\"_blank\">Manufacturers</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/Manufacturers/?$expand=Cars\" target=\"_blank\">Manufacturers/?$expand=Cars</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/Cars\" target=\"_blank\">Cars</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/Cars/?$expand=Driver\" target=\"_blank\">Cars/?$expand=Driver</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/Drivers\" target=\"_blank\">Drivers</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/Drivers/?$expand=Car\" target=\"_blank\">Drivers/?$expand=Car</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/Drivers/?$orderby=Lastname\" target=\"_blank\">Drivers/?$orderby=Lastname</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/Drivers/?$filter=year(Birthday)%20gt%201981\" target=\"_blank\">Drivers/?$filter=year(Birthday) gt 1981</a></li>\n");
      out.write("                </ul>\n");
      out.write("                <h3>Entities</h3>\n");
      out.write("                <ul>\n");
      out.write("                    <li><a href=\"odata.svc/Manufacturers('1')\" target=\"_blank\">Manufacturers('1')</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/Manufacturers('1')/Cars\" target=\"_blank\">Manufacturers('1')/Cars</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/Cars('1')\" target=\"_blank\">Cars('1')</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/Cars('1')/Driver\" target=\"_blank\">Cars('1')/Driver</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/Cars('1')/?$expand=Manufacturer\" target=\"_blank\">Cars('1')/?$expand=Manufacturer</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/Drivers(1)\" target=\"_blank\">Drivers(1)</a></li>\n");
      out.write("                    <li><a href=\"odata.svc/Drivers(1)/Car\" target=\"_blank\">Drivers(1)/Car</a></li>\n");
      out.write("                </ul>\n");
      out.write("            </td>\n");
      out.write("        </tr>\n");
      out.write("        <tr>\n");
      out.write("            <td valign=\"bottom\">\n");
      out.write("                <div class=\"code\">\n");
      out.write("                    ");

                        if (request.getParameter("genSampleData") != null) { //genSampleData is the name of your button, not id of that button.
                            String requestUrl = request.getRequestURL().toString();
                            if(requestUrl.endsWith("index.jsp")) {
                                requestUrl = requestUrl.substring(0, requestUrl.length()-9);
                            }
                            org.apache.olingo.sample.annotation.util.AnnotationSampleDataGenerator.generateData(requestUrl + "odata.svc");
                            response.sendRedirect(requestUrl);
                        }
                    
      out.write("\n");
      out.write("                    <form method=\"POST\">\n");
      out.write("                        <div>\n");
      out.write("                            For generation of sample data this button can be used.\n");
      out.write("                            <br/>\n");
      out.write("                            But be aware that multiple clicking results in multiple data generation.\n");
      out.write("                        </div>\n");
      out.write("                        <input type=\"submit\" id=\"genSampleData\" name=\"genSampleData\" value=\"Generate sample Data\"/>\n");
      out.write("                    </form>\n");
      out.write("                </div>\n");
      out.write("            </td>\n");
      out.write("        </tr>\n");
      out.write("    </table>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
