# Sample OData Service
This is a simple sample odata service which is based on the olingo cars service sample: https://olingo.apache.org/doc/odata2/sample-setup.html

## How to run it locally
To run it locally clone this repository and execute the following steps:

```sh
$ mvn clean install
$ mvn jetty:run
```

Visit http://localhost:8080/

## How to extend it
1. Update the data model in the package **org.apache.olingo.sample.annotation.model**
2. Adjust **AnnotationSampleDataGenerator.class** which generates the sample data if required

## How to deploy it to cloud foundry
First install the cloud foundry cli: https://docs.cloudfoundry.org/cf-cli/install-go-cli.html

Then perform the following commands:
```
$ cf login -a https://api.cf.eu10.hana.ondemand.com -o <org-name> -s <space-name>
$ cf push
```

Execute **cf apps** to list the running applications. There you can see the url of the pushed odata service.

